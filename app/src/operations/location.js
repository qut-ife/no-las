'use strict';

module.exports.parse = (row) => {

  let dd = 0;
  let mm = 0;
  let ss = 0;

  const dms = row.split(' ');

  dd = parseFloat(dms[0]);
  mm = parseFloat(dms[1]);
  ss = parseFloat(dms[2]);

  let decimals = dms[0].split('.');

  if (decimals[1] && parseFloat(decimals[1].length) >= 6) {
    return {
      ddFF: dd
    }
  } else {
    return {
      dd: dd,
      mm: mm,
      ss: ss
    }
  }
};

module.exports.dmsToDD = (dd, mm, ss) => {
  if (mm < 0) mm = -mm;
  if (ss < 0) ss = -ss;
  if (dd < 0 || dd === '-0') {
    mm = -mm;
    ss = -ss;
  }
  return this.roundnum(parseFloat(dd) + parseFloat(mm) / parseFloat(60) + ss / 3600, 6);
};

module.exports.roundnum = (x, p) => {
  let i, j;
  let n = parseFloat(x);
  let m = n.toPrecision(p + 1);
  let y = String(m);
  i = y.indexOf('e');
  if (i === -1)
    i = y.length;
  j = y.indexOf('.');
  if (i > j && j !== -1) {
    while (i > 0) {
      if (y.charAt(--i) === '0')
        y = this.removeAt(y, i);
      else
        break;
    }
    if (y.charAt(i) === '.')
      y = this.removeAt(y, i);
  }
  return y;
};

module.exports.removeAt = (s, i) => {
  s = s.substring(0, i) + s.substring(i + 1, s.length);
  return s;
};

module.exports.parseLoc = (location) => {
  const lA = location.split(/\s/);

  const pushA = [];
  let acc = [];
  lA.forEach((l) => {
    acc.push(l);
    if (l.match(/[NSWE]/)) {
      pushA.push(acc);
      acc = [];
    }
  });

  const coords = {};
  pushA.map((p) => {
    let isNS = p.filter(el => /[NS]/.test(el)).length > 0;
    let parse = this.parse(p.join(' '));
    const val = this.dmsToDD(parse.dd, parse.mm, parse.ss);
    if (isNS) {
      coords.lat = val;
    } else {
      coords.lng = val;
    }
  });

  return coords;
};
