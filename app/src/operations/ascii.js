'use strict';

// Read a LAS line and convert it into an object

const log = require('../logger')();

function unwrapCurve(ascii, startI, curves) {
  //total ascii
  //curves quantities
  try {
    let wrap = {
      row: [],
      next: null
    };

    while (curves > 0) {
      let line = ascii[startI].match(/\S+/g);
      wrap.row = wrap.row.concat(line);
      curves = curves - line.length;
      startI++;
      wrap.next = startI;
    }
    //will return an array of rows starting with dept as first column
    return wrap;
  } catch (error) {
    const msg = 'error in operations.ascii.unwrapCurve: ' + error;
    log.error(msg);
    throw new Error(msg);
  }
}

module.exports.unwrapCurve = unwrapCurve;

module.exports.wrapToRows = function (ascii, curves) {
  let startI = 0;
  let rows = [];
  try {
    while (startI < ascii.length) {
      if (ascii[startI]) {
        let unwrap = unwrapCurve(ascii, startI, curves);
        startI = unwrap.next;
        rows.push(unwrap.row);
      } else {
        startI++;
      }
    }
    return rows;
  } catch (error) {
    const msg = 'error in operations.ascii.wrapToRows: in index:' + startI + ', ' + error;
    log.error(msg);
    throw new Error(msg);
  }

};

module.exports.transpose = function (ascii, el, startI, endI, isWrap) {

  try {
    const arr = [];
    for (let i = startI; i < endI; i++) {
      let line = null;
      if (isWrap) {
        line = ascii[i];
      } else {
        line = ascii[i].match(/\S+/g);
      }
      if (line) {
        arr.push(line[el] || '');
      }
    }
    return arr;
  } catch (error) {
    const msg = 'error in operations.ascii.transpose: ' + error;
    log.error(msg);
    throw new Error(msg);
  }

};
