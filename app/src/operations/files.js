'use strict';

let fs = require('fs');

const walk = function (dir, done) {

  //From directory, recursively find all files and array of folderLocations

  let results = [];

  fs.readdir(dir, function (err, list) {

    if (err) return done(err);
    let i = 0;
    (function next() {

      let file = list[i++];
      if (!file) return done(null, results);
      file = dir + '/' + file;

      fs.stat(file, function (err, stat) {
        if (stat && stat.isDirectory()) {
          walk(file, function (err, res) {
            results = results.concat(res);
            next();
          });
        } else {
          results.push(file);
          next();
        }
      });

    })();

  });

};

module.exports.walk = walk;

module.exports.removeExtension = function (array, extension) {

  const reg = new RegExp('^(.*\\.((' + extension + ')$))*$', 'ig');

  return array.filter((element) => {

    return element.match(reg);

  });

};
