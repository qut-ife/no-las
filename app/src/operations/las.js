'use strict';

const assert = require('assert');
const esrever = require('esrever');

module.exports.validate = (las, ascii, fileLocation) => {

  //Validate mandatory LAS sections
  // ~V, ~A, ~W, ~C
  const message = 'Invalid LAS file, Section: ';

  assert(ascii.length > 0, message + '~A or ~ASCII. ' + fileLocation);

  assert(las.version.length > 0, message + '~V or ~VERSION not found.' + fileLocation);
  assert(this.uniqueItems(las.version), message + '~V or ~VERSION has duplicate elements' + fileLocation);
  assert(las.well.length > 0, message + '~W or ~WELL Not Found' + fileLocation);
  assert(this.uniqueItems(las.well), message + '~V or ~VERSION has duplicate elements' + fileLocation);
  assert(las.curve.length > 0, message + '~C or ~CURVE not found' + fileLocation);
  assert(this.uniqueItems(las.well), message + '~V or ~VERSION has duplicate elements ' + fileLocation);

  return true;
};

module.exports.uniqueItems = (array) => {
  //Object must have unique elements in the array,
  // otherwise could reflect that the file is corrupt

  const valueArr = array.map(function (item) {
    return item.mnem
  });
  const isDuplicate = valueArr.some(function (item, idx) {
    return valueArr.indexOf(item) !== idx
  });

  return !isDuplicate;
};

module.exports.getSection = function (line) {

  let section = '';
  if (line.startsWith('~')) {
    section = line.substring(0, 2);
    switch (section) {
      case '~V':
        section = 'version';
        break;
      case '~W':
        section = 'well';
        break;
      case '~C':
        section = 'curve';
        break;
      case '~P':
        section = 'parameter';
        break;
      case '~O':
        section = 'other';
        break;
      case '~A':
        section = 'ascii';
        break;
      default:
        section = null;
    }

    return section;

  }
};

module.exports.getMNEM = function (line) {

  const reg = new RegExp(/./g);
  let lineM = line.match(reg);

  let mnem = '';
  let mnemR = null;

  for (let i = 0; i < lineM.length; i++) {
    if (lineM[i] === '.') {
      mnemR = i + 1;
      break;
    } else {
      mnem += lineM[i];
    }
  }

  let units = '';
  let unitsR = null;

  for (let i = mnemR; i < lineM.length; i++) {

    if (lineM[i] === ' ') {
      unitsR = i + 1;
      break;
    } else {
      units += lineM[i];
    }
  }

  let desc = '';
  let descR = lineM.length - 1;
  for (let i = descR; i >= 0; i--) {
    if (lineM[i] === ':') {
      descR = i;
      desc = esrever.reverse(desc);
      break;
    } else {
      desc += lineM[i];
    }
  }

  let data = '';
  for (let i = unitsR; i < descR; i++) {
    data += lineM[i];
  }


  return {
    mnem: mnem.trim(),
    units: units.trim(),
    data: data.trim(),
    desc: desc.trim()
  }
};

module.exports.mnemOfArray = function (array, mnem) {

  return array.find((obj) => {
    return obj.mnem === mnem;
  });

};
