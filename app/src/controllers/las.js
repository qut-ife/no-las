'use strict';

const log = require('../logger')();
const lasModel = require('../models/las');
const utils = require('../utils/utils');

module.exports.create = () => {

  const entry = new lasModel({
    status: 'new',
    version: [],
    well: [],
    curve: [],
    parameter: [],
    other: [],
    ascii: []
  });

  return entry
    .save()
    .then((res) => {
      return res;
    })
    .catch((error) => {
      log.error('las create error: ' + error);
      return Promise.reject('las create error: ' + error);
    });
};

module.exports.getById = (id) => {

  const query = lasModel.findOne({_id: id}).exec();

  return query
    .then((result) => {
      return result;
    })
    .catch((error) => {
      log.error('las getById error:' + error);
      return Promise.reject('las getById error: ' + error);
    });

};

module.exports.insert = (id, data, isUpsert) => {

  let size = utils.strToMB(data);
  size = size <= 0 ? utils.strToKB(data) + ' Kb' : size + ' Mb';
  const msg = 'size of las: ' + size;
  log.debug(msg);
  if (log.level.level <= 6000) {
    console.log(msg);  // eslint-disable-line no-console
  }

  data.status = 'success';

  const query = lasModel.findOneAndUpdate({_id: id}, {$set: data}, {upsert: isUpsert}).exec();

  return query
    .then((result) => {
      return result;
    })
    .catch((error) => {
      const msg = 'las.insert error: ' + error;
      log.error(msg);
      return Promise.reject(msg);
    });

};
