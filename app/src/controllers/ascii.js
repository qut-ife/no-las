'use strict';

const log = require('../logger')();
const asciiModel = require('../models/ascii');
const utils = require('../utils/utils');

module.exports.create = () => {

  const entry = new asciiModel({
    las_id: String,
    order: Number,
    ascii: [],
    createdOn: Date.now(),
    updatedOn: Date.now()
  });

  return entry
    .save()
    .then((res) => {
      return res;
    })
    .catch((error) => {
    const msg = 'ascii.create error: ' + error;
      log.error(msg);
      return Promise.reject(msg);
    });
};

module.exports.getById = (id) => {

  const query = asciiModel.findOne({_id: id}).exec();

  return query
    .then((result) => {
      return result;
    })
    .catch((error) => {
    const msg = 'ascii.getById error:' + error;
      log.error(msg);
      return Promise.reject(msg);
    });

};


module.exports.insert = (id, data, isUpsert) => {

  let size = utils.strToMB(data);
  size = size <= 0 ? utils.strToKB(data) + ' Kb' : size + ' Mb';
  const msg = 'size of ascii part '+ data.order + ': ' + size;
  log.debug(msg);

  if (log.level.level <= 6000) {
    console.log(msg); // eslint-disable-line no-console
  }

  data.updatedOn = Date.now();
  data.createdOn = Date.now();
  data.status = 'success';

  const query = asciiModel.findOneAndUpdate({_id: id}, {$set: data}, {upsert: isUpsert}).exec();

  return query
    .then((result) => {
      let msg = 'ascii.insert: ' + id + ' order: ' + data.order + ' of ' + data.total;
      log.debug(msg);
      if(log.level.level <= 6000){
        console.log(msg); // eslint-disable-line no-console
      }
      return result;
    })
    .catch((error) => {
      const msg = 'ascii.insert error: ' + error;
      log.error(msg);
      return Promise.reject(msg);
    });

};

