'use strict';

const log = require('../logger')();
const utils = require('../utils/utils');
const lineReader = require('line-reader');
const asciiOps = require('../operations/ascii');
const lasOps = require('../operations/las');

module.exports.lineByLine = function (fileLocation, iteratee) {

  return new Promise(function (resolve, reject) {
    lineReader.eachLine(fileLocation, {}, iteratee, function (err) {
      if (err) {
        log.error('lineByLine error: ' + fileLocation);
        reject(err);
      } else {
        resolve();
      }
    });
  });
};

module.exports.las = function (prelas, include) {

  let version = prelas.version
    .filter(e => e !== '')
    .map((line) => {
      return lasOps.getMNEM(line);
    });

  let well = prelas.well
    .filter(e => e !== '')
    .map((line) => {
      return lasOps.getMNEM(line);
    });

  let curve = prelas.curve
    .filter(e => e !== '')
    .map((line) => {
      return lasOps.getMNEM(line);
    });

  let parameter = [];
  if (include.parameter) {
    parameter = prelas.parameter
      .filter(e => e !== '')
      .map((line) => {
        return lasOps.getMNEM(line);
      });
  }

  let other = [];
  if (include.other) {
    other = prelas.other
      .filter(e => e !== '')
      .map((line) => {
        return line; //Insert line as is
      });
  }

  return {
    status: 'validate',
    file: prelas.file,
    version: version,
    well: well,
    curve: curve,
    parameter: parameter,
    other: other
  }

};

module.exports.ascii = (id, curve, asciis, isWrap, curves) => {

  if (isWrap) {
    asciis = asciiOps.wrapToRows(asciis, curves);
  }
  const mb = utils.strToMB(asciis);
  let bsons = 1;
  if (mb > 9) {
    bsons = utils.divideBy(mb, 9);
  }

  log.debug('Creating ' + bsons + ' bson(s)');

  const partialIndex = Math.ceil(asciis.length / bsons);

  const parts = [];
  let startI = 0;
  let endI = partialIndex;

  for (let order = 1; order <= bsons; order++) {
    parts.push(part(id, order, bsons, asciis, startI, endI, curve, isWrap));
    startI = endI + 1;
    let end = endI + partialIndex;
    endI = end > asciis.length ? asciis.length : end;
  }

  return parts;

};

function part(id, order, bsons, ascii, startI, endI, curve, isWrap) {

  let partialAscii = [];

  curve.forEach((cur, curIndex) => {

    let eachCurve = {
      mnem: cur.mnem,
      data: asciiOps.transpose(ascii, curIndex, startI, endI, isWrap)
    };

    partialAscii.push(eachCurve);

  });

  return {
    las_id: id,
    order: order,
    total: bsons,
    ascii: partialAscii
  }

}
