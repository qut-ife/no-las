'use strict';

const fs = require('fs');
const log = require('../logger')();
const filesModel = require('../models/file');

module.exports.create = (fileLocation) => {

  const entry = new filesModel({
    fileLocation: fileLocation,
    status: 'new',
    process: 'ready',
    info: fs.statSync(fileLocation)
  });

  return entry
    .save()
    .then((res) => {
      return res;
    })
    .catch((error) => {
      log.error('files.create error: ' + error);
      return Promise.reject('files create error: ' + error);
    });
};

module.exports.getById = (id) => {

  const query = filesModel.findOne({_id: id}).exec();

  return query
    .then((result) => {
      return result;
    })
    .catch((error) => {
      log.error('files.getById error:' + error);
      return Promise.reject('files getById error: ' + error);
    });

};

module.exports.insert = (id, data, isUpsert) => {

  const query = filesModel.findOneAndUpdate(
    {_id: id},
    {$set: data},
    {
      upsert: isUpsert,
      setDefaultsOnInsert: true
    }).exec();

  return query
    .then((result) => {
      return result;
    })
    .catch((error) => {
      log.error('files.insert error: ' + error);
      return Promise.reject('files insert error: ' + error);
    });

};

module.exports.updateByName = (name, status, process) => {

  const query = filesModel.findOneAndUpdate(
    {fileLocation: name},
    {
      $set: {
        status: status,
        process: process,
        updatedOn: Date.now()
      }
    },
    {upsert: false}
  ).exec();

  return query
    .then((result) => {
      return result;
    })
    .catch((error) => {
      const msg = 'files.updateByName error: ' + error;
      log.error(msg);
      return Promise.reject(msg);
    });

};

module.exports.updateById = (id, status, process) => {

  const query = filesModel.findOneAndUpdate(
    {_id: id},
    {
      $set: {
        status: status,
        process: process,
        updatedOn: Date.now()
      }
    },
    {upsert: false}
  ).exec();

  return query
    .then((result) => {
      return result;
    })
    .catch((error) => {
      const msg = 'files.updateById error: ' + error;
      log.error(msg);
      return Promise.reject(msg);
    });

};

module.exports.bulk = (docs) => {

  return new Promise((resolve, reject) => {
    const fileLocations = docs.map((fileLocation) => {
      return new filesModel({
        fileLocation: fileLocation,
        status: 'new',
        process: 'ready',
        info: fs.statSync(fileLocation),
        createdOn: Date.now(),
        updatedOn: Date.now(),
        __v: 0
      })._doc;
    });

    filesModel.collection.insert(
      fileLocations,
      {ordered: true},
      (err, docs) => {
        if (err) {
          reject(err)
        } else {
          resolve(docs);
        }
      });

  }).then((result) => {
    return result;
  })
    .catch((error) => {
      const msg = 'files.bulk insert error: ' + error;
      log.error(msg);
      return Promise.reject(msg);
    });
};

module.exports.getProcess = (process) => {

  const query = filesModel.find({process: {$ne: process}}).exec();

  return query
    .then((res) => {
      return res;
    })
    .catch((error) => {
      const msg = 'files.getProcess error: ' + error;
      log.error(msg);
      return Promise.reject(msg);
    });

};
