/* eslint no-console:0*/
'use strict';

const assert = require('assert');

const settings = require('../../settings');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const files = require('../controllers/files');
const filesOps = require('../operations/files');

let lasNonProcessed = []; // eslint-disable-line no-unused-vars
let dir = process.argv[2] || null;
let removed = [];

assert.equal(!dir, false);

//get all files recursively in a directory',

const recurseFiles = function (done) {

  console.time('createBulkFiles');
  filesOps.walk(dir, (error, fileLocations) => {
    if (error) throw error;
    removed = filesOps.removeExtension(fileLocations, 'las');
    files
      .bulk(removed)
      .then((res) => {
        console.timeEnd('createBulkFiles');
        done();
      })
      .catch((error) => {
        assert.equal(error, null);
        done();
      })
  });

};

//get all files that havent been processed

const getProcessed = function (done) {

  console.time('getProcessed');

  files
    .getProcess('processed')
    .then((res) => {
      lasNonProcessed = res;
      console.timeEnd('getProcessed');
      done();
    })
    .catch((error) => {
      assert.equal(error, null);
      return Promise.reject();
    });

};


recurseFiles(() => {
  getProcessed(() => {
    console.log('finish');
    process.exit();
  });
});
