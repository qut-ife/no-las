'use strict';

const assert = require('assert');
const log = require('../logger')();
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const parse = require('../controllers/parse');
const las = require('../controllers/las');
const lasOps = require('../operations/las');
const ascii = require('../controllers/ascii');

module.exports.import = (oneFile, include) => {

  let id = null;

  let amend = {
    file: oneFile.fileLocation,
    version: [],
    well: [],
    curve: [],
    parameter: [],
    other: [],
    ascii: []
  };

  let curve = [];
  let version = [];

  return new Promise((resolve, reject) => {
    log.debug('process import las');
    const start = new Date().getTime();
    las
      .create()
      .then((res) => {
        id = res.id;
        let previousSection = null;
        log.debug('parse line by line');
        parse.lineByLine(oneFile.fileLocation, function (line, last) {
          let section = lasOps.getSection(line);
          if (section) {
            if (previousSection !== section) {
              previousSection = section;
            }
          }
          if (previousSection !== section && previousSection === 'other') {
            amend[previousSection].push(line);
          } else if (previousSection !== section && !line.startsWith('#') && previousSection !== null) {
            amend[previousSection].push(line);
          }
          if (last) {
            log.debug('last line of file: ' + line);
          }
        })
          .then(() => {
            log.debug('parse las');
            return parse.las({
                status: 'new',
                file: amend.file,
                version: amend.version,
                curve: amend.curve,
                other: amend.other,
                parameter: amend.parameter,
                well: amend.well
              },
              {
                parameter: include.parameter,
                other: include.other
              }
            );
          })
          .then((res) => {
            res.file_id = oneFile.id.toString();
            curve = res.curve;
            version = res.version;
            log.debug('insert las of file_id: ' + res.file_id);
            assert(lasOps.validate(res, amend.ascii, oneFile.fileLocation));
            return las.insert(id, res);
          })
          .then((res) => {
            log.debug('parse partials with las_id: ' + res.id);
            const wrap = lasOps.mnemOfArray(version, 'WRAP');
            const isWrap = wrap.data.toUpperCase() === 'YES';
            return parse.ascii(res.id, curve, amend.ascii, isWrap, curve.length);
          })
          .then((res) => {
            return res.map((asciiObject) => {
              return ascii.insert(mongoose.Types.ObjectId(), asciiObject, true);
            });
          })
          .then((res) => {
            log.debug('ascii promise reduce');
            return res.reduce((p, f) => {
              return p
                .then(f)
                .catch((err) => {
                  let msg = 'ascii promise all part error' + err;
                  console.log(msg); // eslint-disable-line no-console
                  reject(msg);
                });
            }, Promise.resolve());
          })
          .then((res) => {
            const end = new Date().getTime();
            const t = end - start;
            log.debug('processing time: ' + t + 'ms');
            log.info('finish');
            resolve(res);
          })
          .catch((error) => {
            log.error('error in process.LAS.import: ' + oneFile.fileLocation + ' ' + error);
            reject(error);
          });
      });
  });
};
