/* eslint no-console:0*/
'use strict';

const settings = require('../../settings');
const mongoose = require('mongoose');
const assert = require('assert');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const fileModel = require('../models/file');

console.time('clear');

//TODO: add warning to delete all files

fileModel.remove()
  .then((res) => {
    assert.equal(res.result.ok, 1);
    console.log('files in database deleted');
    console.timeEnd('clear');
    process.exit();
  })
  .catch((error) => {
    assert.equal(error, null);
    process.exit();
  });


