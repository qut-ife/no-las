/* eslint no-console:0*/
'use strict';

const settings = require('../../settings');
const mongoose = require('mongoose');
const assert = require('assert');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const asciiModel = require('../models/ascii');
const lasModel = require('../models/las');

console.time('clear');

//TODO: add warning to delete all las and asciis

asciiModel.remove()
  .then(()=>{
    console.log('asciis deleted');
    return lasModel.remove();
  })
  .then((res) => {
    assert.equal(res.result.ok, 1);
    console.log('las deleted');
    console.timeEnd('clear');
    process.exit();
  })
  .catch((error) => {
  assert.equal(error, null);
  });


