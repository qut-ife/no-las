'use strict';

const log = require('../logger')();
const settings = require('../../settings');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const files = require('../controllers/files');
const imports = require('./imports');

const oParam = process.argv[2].toLowerCase() === 'yes';
const oOther = process.argv[3].toLowerCase() === 'yes';

const getFiles = (cb) => {

  files
    .getProcess('processed')
    .then((lasNonProcessed) => {
      cb(null, lasNonProcessed)
    })
    .catch((error) => {
      log.error(error);
      cb(error, null);
    });

};

getFiles((err, lasNonProcessed) => {

  if (err) {
    console.log(err); // eslint-disable-line no-console
    log.error(err);
  } else {
    imports.start(lasNonProcessed, {parameter: oParam, other: oOther});
  }

});
