/* eslint no-console:0*/
'use strict';

//Use coords: lat, lng, loc to create a table location: that has lat, lng

const settings = require('../../settings');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const coordsModel = require('../models/coords');
const locationModel = require('../models/location');
const locationOps = require('../operations/location');
console.time('clean');

const coordsQuery = coordsModel.find().exec();

coordsQuery.then((coords) => {

  //with all coords, clean each and insert.
  const allCoords = [];
  coords.map((coord) => {
    let newCoord = {};
    if (coord._doc.lat && coord._doc.lng) {
      let dms = locationOps.parse(coord._doc.lat);
      let ddFF = locationOps.dmsToDD(dms.dd, dms.mm, dms.ss);
      newCoord.lat = ddFF;
      dms = locationOps.parse(coord._doc.lng);
      ddFF = locationOps.dmsToDD(dms.dd, dms.mm, dms.ss);
      newCoord.lng = ddFF;
    }
    else if (!newCoord.lat || !newCoord.lng) {
      const newLoc = locationOps.parseLoc(coord._doc.loc);
      newCoord.lat = newLoc.lat;
      newCoord.lng = newLoc.lng;
    }

    newCoord.las_id = coord._doc.las_id;
    allCoords.push(newCoord);
  });

  locationModel.collection.insert(
    allCoords,
    {ordered: true},
    (err, docs) => {
      if (err) {
        Promise.reject(err)
      } else {
        Promise.resolve(docs);
      }
    });

}).then((res) => {
  console.log(res);
}).catch((err) => {
  console.log(err);
});
