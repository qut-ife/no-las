/* eslint no-console:0*/
'use strict';

const log = require('../logger')();
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const processLAS = require('./LAS');
const files = require('../controllers/files');
const EventEmitter = require('events');

class MyEmitter extends EventEmitter {
}

const processing = new MyEmitter();
let lasFiles = [];
const include = {
  parameter: false,
  other: false
};

let i = 0;

module.exports.start = (LASFiles, inc) => {

  lasFiles = LASFiles;
  include.parameter = inc.parameter;
  include.other = inc.other;
  processing.emit('next');
};

processing.on('next', () => {
  console.time('next');
  if (lasFiles[i] === undefined) {
    processing.removeListener('next', () => {
      console.log('removed');
    });
    console.timeEnd('next');
    finish();
  } else {
    each(lasFiles[i], (err, res) => { // eslint-disable-line no-unused-vars
      if (err !== null) {
        let msg = lasFiles[i] + ': ' + err;
        console.log(msg);
        log.error(msg);
      } else {
        let msg = 'processed: ' + i + ': ' + lasFiles[i].fileLocation + '; size: ' + lasFiles[i].info.size;
        log.info(msg);
      }
      i++;
      console.timeEnd('next');
      processing.emit('next');
    });
  }
});

const each = (lasFile, cb) => {
  let msg = 'processing: ' + i + ': ' + lasFile.fileLocation + '; size: ' + lasFile.info.size;
  console.log(msg);
  files
    .updateById(lasFile.id, 'started', 'process started')
    .then((res) => {
      processLAS
        .import(res, include)
        .then(() => {
          return files.updateById(lasFile.id, 'success', 'processed');
        })
        .then((res) => {
          cb(null, res);
        })
        .catch((error) => {
          log.error(error + ' ' + lasFile.fileLocation);
          cb(error, null);
        });

    });

};

const finish = () => {
  console.log('finish');
  process.exit();
};
