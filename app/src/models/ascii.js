const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const Schema = mongoose.Schema;

const ascii = new Schema({
  mnem: String,
  data: [String]
});

const ASCIISchema = new Schema({
  las_id: String,
  order: Number,
  total: Number,
  status: String,
  ascii: [ascii],
  createdOn: {type: Date, default: Date.now},
  updatedOn: {type: Date, default: Date.now}
});

module.exports = mongoose.model('ascii', ASCIISchema);
