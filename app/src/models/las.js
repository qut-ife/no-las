const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const Schema = mongoose.Schema;

const mnem = new Schema({
  mnem: String,
  units: String,
  data: String,
  desc: String
});

const LASSchema = new Schema({
  status: String,
  file: String,
  file_id: String,
  version: [mnem],
  well: [mnem],
  curve: [mnem],
  parameter: [mnem],
  other: [String],
  createdOn: {type: Date, default: Date.now},
  updatedOn: {type: Date, default: Date.now}
});

module.exports = mongoose.model('las', LASSchema);
