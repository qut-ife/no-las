const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const Schema = mongoose.Schema;

const ASCIISchema = new Schema({
  fileLocation: String,
  status: String,
  process: String,
  info: Object,
  createdOn: {type: Date, default: Date.now},
  updatedOn: {type: Date, default: Date.now}
});

module.exports = mongoose.model('file', ASCIISchema);
