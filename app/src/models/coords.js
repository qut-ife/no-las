const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

const Schema = mongoose.Schema;

const LocationSchema = new Schema({
  las_id: Object,
  lat: String,
  lng: String,
  loc: String
});

module.exports = mongoose.model('coords', LocationSchema);
