'use strict';

const log4js = require('log4js');
const settings = require('../settings');

log4js.clearAppenders();
log4js.loadAppender('file');
log4js.addAppender(log4js.appenders.file(settings.log_file), 'main');

module.exports = function () {
  let logger = log4js.getLogger('main');
  logger.setLevel(settings.log_level);
  return logger;
};
