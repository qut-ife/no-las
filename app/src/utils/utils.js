'use strict';

const assert = require('assert');

module.exports.strToMB = (obj) => {

  // 1 MB = 1.19209289550781e-07 bits
  const bits = JSON.stringify(obj).length;
  const mb = (bits * 16) * 1.19209289550781e-07;
  return mb.toFixed(1);

};

module.exports.strToKB = (obj) => {

  // 1 KB = 0.000125 bits
  const bits = JSON.stringify(obj).length;
  const mb = (bits * 16) * 0.000125;
  return mb.toFixed(1);

};

module.exports.strToBinary = (str) => {
  let binary = '';
  for (let i = 0; i < str.length; i++) {
    binary += str[i].charCodeAt(0).toString(2) + ' ';
  }
  return binary;
};


module.exports.divideBy = (size, divider) => {
  //how many db bsons should it produce?

  assert(divider > 0, 'Error division by zero');

  const total = size * 100 / 10;

  return Math.ceil(total / 100);

};


module.exports.PromiseEach = PromiseEach;

// apply a function to all values
// should only be used for side effects
// (fn) -> prom
function PromiseEach(fn) {
  assert.equal(typeof fn, 'function');
  return function (arr) {
    arr = Array.isArray(arr) ? arr : [arr];

    return arr.reduce(function (prev, curr, i) {
      return prev.then(function () {
        return fn(curr, i, arr.length)
      })
    }, Promise.resolve()).then(function () {
      return arr
    })
  }
}

module.exports.roughSize = (object) => {

  let objectList = [];
  let stack = [object];
  let bytes = 0;

  while (stack.length) {
    let value = stack.pop();

    if (typeof value === 'boolean') {
      bytes += 4;
    }
    else if (typeof value === 'string') {
      bytes += value.length * 2;
    }
    else if (typeof value === 'number') {
      bytes += 8;
    }
    else if (
      typeof value === 'object'
      && objectList.indexOf(value) === -1
    ) {
      objectList.push(value);

      for (let i in value) {
        stack.push(value[i]);
      }
    }
  }

  return (bytes * 1e-6).toFixed();

};
