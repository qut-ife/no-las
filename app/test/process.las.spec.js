'use strict';

const log = require('../src/logger')();
const settings = require('../settings');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const assert = require('assert');

const processLAS = require('../src/process/LAS');

const fileDir = process.cwd() + '/test/test-data/';
let fileLocation = '';
fileLocation = fileDir + 'BLD HALIFAX 1_mem 2608-4267m.las'; //3Mb
fileLocation = fileDir + 'ROOKWOOD_CENTRAL_1_PROCESSED.LAS';
fileLocation = fileDir + 'Mainlog10CM.las';
fileLocation = fileDir + 'QGC_BORROWDALE_2_REPEAT.las';
fileLocation = fileDir + '/import/AAP HUT CREEK 1_A.las'; //Wrap YES 6MB
fileLocation = fileDir + '/import/AGA CUBAN 1 LAS_1.las'; //Wrap NO 18MB
fileLocation = fileDir + 'QGC_AVON_DOWNS_9_REPEAT.las'; //Empty unrwapped rows caused to crash
fileLocation = fileDir + 'SSL KANANDA 1_A_PART1.las'; //file has multiple LAS file appended in it
fileLocation = fileDir + 'QGC_AVON_DOWNS_9_HIGH_RES.las'; //Not valid ASCII
fileLocation = fileDir + 'DM42_Density_D.LAS'; //No ~ASCII or ~A AssertionError
fileLocation = fileDir + 'BLD HALIFAX 1_mem 2608-4267m.las';
fileLocation = fileDir + 'AAO PLEASANT HILLS 4_USIT-CBL-GR-CCL-HighResPass.las'; //47Kb
fileLocation = fileDir + 'ROOKWOOD_CENTRAL_1_PROCESSED.LAS';
fileLocation = fileDir + 'SSL MUNKAH 10_SNAM 1.las';
fileLocation = fileDir + 'OCA CORRUMBENE 1_A.las';

let oneFile = {
  id: mongoose.Types.ObjectId(),
  fileLocation: fileLocation
};

describe('Operations', function () {

  it('should insert LAS and ASCIIs', function () {

    log.info('importing: ' + fileLocation);
    const start = new Date().getTime();

    return processLAS
      .import(oneFile, {parameter: true, other: true})
      .then((res) => {
        const end = new Date().getTime();
        const t = end - start;
        log.debug('import time: ' + t + 'ms');
        assert(res === undefined);
      })
      .catch((error) => {
        const end = new Date().getTime();
        const t = end - start;
        log.error('error import time: ' + t + 'ms: ' + error);
        return Promise.reject(error);
      });

  });

});
