'use strict';

const assert = require('assert');

const settings = require('../settings');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const fileModel = require('../src/models/file');
const files = require('../src/controllers/files');

describe('Import', function () {

  let lasNonProcessed = []; // eslint-disable-line no-unused-vars
  let dir = settings.las_dir;
  let removed = [];

  it('should delete all files', function (done) {

    fileModel.remove({}, function (err, res) { // eslint-disable-line no-unused-vars
      assert.equal(err, null);
      done();
    });

  });

  it('should get all files recursively in a directory', function (done) {

    console.time('createBulkFiles'); // eslint-disable-line no-console
    files.walk(dir, (error, fileLocations) => {
      if (error) throw error;

      removed = files.removeExtension(fileLocations, 'las');
      files
        .bulk(removed)
        .then((res) => {
          console.timeEnd('createBulkFiles'); // eslint-disable-line no-console
          //console.log(util.inspect(res.insertedCount, false, null))
          assert(res.insertedCount, removed.length);
          done();
        })
        .catch((error) => {
          assert.equal(error, null);
          done();
        })

    });

  });

  it('should get all files that havent been processed', function (done) {
    console.time('getProcessed'); // eslint-disable-line no-console

    files
      .getProcess('processed')
      .then((res) => {
        lasNonProcessed = res;
        console.timeEnd('getProcessed'); // eslint-disable-line no-console

        assert.equal(removed.length, res.length);
        done();
      });
  });


});
