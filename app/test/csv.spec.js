'use strict';

const assert = require('assert');
const csv = require('ya-csv');
const fs = require('fs');

describe('write csv file', function () {

  it('should write a csv file', function () {
    const fileName = 'test/test-data/file.csv';
    const writer = csv.createCsvFileWriter(fileName, {'flags': 'a'});
    const csvData = new Array();
    const id = '1';
    const name = 'moises';
    csvData.push(id);
    csvData.push(name);
    writer.writeRecord(['id', 'name']);
    writer.writeRecord(csvData);
    writer.writeStream.end(); // this is how to close the file descriptor

    const reader = csv.createCsvFileReader(fileName, {columnsFromHeader: true});
    reader.addListener('data', (data) => {
      console.log(data);
      fs.unlink(fileName, () => {
        assert.deepEqual({id: '1', name: 'moises'}, data);
      });
    });
  });

});
