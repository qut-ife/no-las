'use strict';

const assert = require('assert');

const settings = require('../settings');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const files = require('../src/controllers/files');
const fileModel = require('../src/models/file');


describe('Files', function () {

  it('should delete all files', function () {

    fileModel.remove({}, function (err, res) { // eslint-disable-line no-unused-vars
      assert.equal(err, null);
    });

  });

  it('should create a file in the database', function () {
    console.log(''); // eslint-disable-line no-console
    console.time('createFile'); // eslint-disable-line no-console

    const fileLocation = '/Users/moy/Source/Node/no-las/app/test/test-data/BLD HALIFAX 1_mem 2608-4267m.las';

    files
      .create(fileLocation)
      .then((res) => {
        //console.log(util.inspect(res, false, null))
        const fileLocation = '/Users/moy/Source/Node/no-las/app/test/test-data/BLD HALIFAX 1_mem 2608-4267m.las';
        console.timeEnd('createFile'); // eslint-disable-line no-console

        assert.equal(res.fileLocation, fileLocation);
      });

  });

  it('should update a file in the database', function () {
    console.time('updateFile'); // eslint-disable-line no-console

    const fileLocation = '/Users/moy/Source/Node/no-las/app/test/test-data/BLD HALIFAX 1_mem 2608-4267m.las';
    const status = 'updated';
    const process = 'ready';

    files
      .updateByName(fileLocation, status, process)
      .then((res) => {
        console.timeEnd('updateFile'); // eslint-disable-line no-console
        //console.log(util.inspect(res, false, null))

        assert.equal(res.status, status);
      });

  });

  it('should insert in bulk a list of files in the database', function () {
    console.log(''); // eslint-disable-line no-console
    console.time('createBulkFiles'); // eslint-disable-line no-console

    const removed = [
      '/Users/moy/Source/Node/no-las/app/test/test-data/AAO PLEASANT HILLS 4_USIT-CBL-GR-CCL-HighResPass.las'
    ];
    files
      .bulk(removed)
      .then((res) => {
        console.timeEnd('createBulkFiles'); // eslint-disable-line no-console
        //console.log(util.inspect(res.insertedCount, false, null))
        assert.equal(res.insertedCount, removed.length);
      });

  });

  it('should find files that have processed', function () {
    console.time('findFiles'); // eslint-disable-line no-console
    files
      .getProcess('done')
      .then((res) => {
        console.timeEnd('findFiles'); // eslint-disable-line no-console
        assert.equal(res.length, 2);
      });
  });

});
