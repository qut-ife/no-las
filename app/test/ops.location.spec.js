'use strict';

const assert = require('assert');

const locationOps = require('../src/operations/location');


//Helper: http://www.sunearthtools.com/dp/tools/conversion.php

describe('Parse and extract several ways the location is in the DB', () => {

  const coords = [
    "25 38' 48.5'' S",
    "25* 43' 27.1\" S",
    "025 03' 53.900\" S    DMS",
    "25� 24' 13.81\" S",
    "26.457437* S"
  ];

  it('Should parse DMS from string to Decimal Degrees (dd.ff) (4)', () => {
    const dms = locationOps.parse(coords[4]);
    const ddFF = dms.ddFF;
    assert.deepEqual(ddFF, 26.457437);
  });

  it('Should parse DMS from string to Decimal Degrees (dd.ff) (1)', () => {
    const dms = locationOps.parse(coords[0]);
    assert.deepEqual(dms, {dd: 25, mm: 38, ss: 48.5});
  });

  it('Should parse DMS from string to Decimal Degrees (dd.ff) (2)', () => {
    const dms = locationOps.parse(coords[1]);
    assert.deepEqual(dms, {dd: 25, mm: 43, ss: 27.1});
  });

  it('Should parse DMS from string to Decimal Degrees (dd.ff) (3)', () => {
    const dms = locationOps.parse(coords[2]);
    assert.deepEqual(dms, {dd: 25, mm: 3, ss: 53.9});
  });

  it('Should parse DMS from string to Decimal Degrees (dd.ff) (3)', () => {
    const dms = locationOps.parse(coords[3]);
    assert.deepEqual(dms, {dd: 25, mm: 24, ss: 13.81});
  });

  it('Should calculate dms to dd', () => {
    //25 38' 48.5'' S
    //25 degrees 38 minutes 48.5 seconds South
    //dd = whole degrees, mm = minutes, ss = seconds
    //dd.ff = dd + mm/60 + ss/3600

    const dms = locationOps.parse(coords[0]);
    const ddFF = locationOps.dmsToDD(dms.dd, dms.mm, dms.ss);
    assert.equal(ddFF, 25.64681);
  });

  const location = "28� 11' 52.9\" S, 141� 47' 26.1\" E";

  // const loc = {
  //   lat: '28.19803',
  //   lng: '141.7906'
  // };

  const l1 = locationOps.parse("28� 11' 52.9\" S");
  const p1 = locationOps.dmsToDD(l1.dd, l1.mm, l1.ss);
  const l2 = locationOps.parse("141� 47' 26.1\" E");
  const p2 = locationOps.dmsToDD(l2.dd, l2.mm, l2.ss);

  it('Should parse location in loc object (1)', () => {
    assert.deepEqual(locationOps.parseLoc(location), {lat: p1, lng: p2});
  });

  it('Should parse location in loc object (2)', () => {
    assert.deepEqual(locationOps.parseLoc(location), {lat: p1, lng: p2});
  });

});
