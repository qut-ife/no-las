'use strict';

const assert = require('assert');
const utils = require('../src/utils/utils');

describe('utils', function () {

  it('should calculate how many bsons to create', function (done) {
    let val = utils.divideBy(23); //23 MB
    assert.equal(val, 2);
    done();
  });

  it('should calculate how many bsons to create (2)', function (done) {
    let val = utils.divideBy(32.1); //32.1 MB
    assert.equal(val, 3);
    done();
  });

});
