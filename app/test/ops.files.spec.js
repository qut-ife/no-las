'use strict';

const assert = require('assert');

const settings = require('../settings');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const filesOps = require('../src/operations/files');
const dir = process.cwd() + '/test/test-data'; // no end back slash

describe('FILEs operations', function () {

  it('should remove all not .las files', function () {
    console.log(''); // eslint-disable-line no-console
    let removed = [];

    console.time('remove .las'); // eslint-disable-line no-console

    const fileLocations = [
      '/Users/moy/Source/Node/no-las/app/test/test-data/a.lass',
      '/Users/moy/Source/Node/no-las/app/test/test-data/b.exe.las',
      '/Users/moy/Source/Node/no-las/app/test/test-data/b.las.some',
      '/Users/moy/Source/Node/no-las/app/test/test-data/c.nolas',
      '/Users/moy/Source/Node/no-las/app/test/test-data/something'
    ];

    removed = filesOps.removeExtension(fileLocations, 'las');

    console.timeEnd('remove .las'); // eslint-disable-line no-console

    assert(removed.length === 1);
  });

  it('should get all files recursively in a directory', function () {

    console.time('recurse'); // eslint-disable-line no-console
    filesOps.walk(dir, (error, fileLocations) => {

      console.log(fileLocations); // eslint-disable-line no-console
      if (error) throw error;

      console.timeEnd('recurse'); // eslint-disable-line no-console
      assert(fileLocations.length >=  16);

    });
  });

});
