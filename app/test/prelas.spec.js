'use strict';

const settings = require('../settings');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const assert = require('assert');
const lasOps = require('../src/operations/las');
const prelas = require('../src/controllers/prelas');
const parse = require('../src/controllers/parse');

const fileDir = process.cwd() + '/test/test-data/';
const fileLocation = fileDir + '0_Origin_Condabri_23_AIT-PEX-GPIT_Main_Pass_Final.las';

describe('pre process las', function () {

  it('should insert new object', function (done) {

    const id = mongoose.Types.ObjectId();
    const update = {status: 'parsing'};
    const isUpsert = true;
    prelas
      .insert(id, update, isUpsert)
      .then(() => {
        return prelas.getById(id);
      })
      .then((res) => {
        assert.equal(res.id, id);
        done();
      })
      .catch((error) => {
        console.log(error); // eslint-disable-line no-console
        done();
      });

  });

  it('should update data', function (done) {

    const id = mongoose.Types.ObjectId();
    let update = {
      status: 'parsing'
    };
    const isUpsert = true;

    prelas
      .insert(id, update, isUpsert)
      .then(() => {
        return prelas.getById(id);
      })
      .then(() => {
        let update = {
          version: ['~VER'],
          well: ['~WELL']
        };
        return prelas.insert(id, update, isUpsert)
      })
      .then((res) => {
        assert.equal(res.id, id);
        done();
      })
      .catch((res) => {
        console.log(res); // eslint-disable-line no-console
        done();
      })

  });

  it('should load each line to process', function (done) {

    let id = null;
    let newId = undefined;
    prelas
      .create()
      .then((res) => {
        id = res.id;
        let previousSection = null;
        let amend = {
          file: fileLocation,
          version: [],
          well: [],
          curve: [],
          parameter: [],
          other: [],
          ascii: []
        };

        parse.lineByLine(fileLocation, function (line, last) { // eslint-disable-line no-unused-vars
          let section = lasOps.getSection(line);
          if (section) {
            if (previousSection !== section) {
              previousSection = section;
            }
          }
          if (previousSection !== section && !line.startsWith('#')) {
            amend[previousSection].push(line);
          }

        })
          .then(() => {
            return prelas.insert(id, amend);
          })
          .then((res) => {
            newId = res.id;
            done();
          })
          .catch((error) => {
            console.log(error); // eslint-disable-line no-console
            done();
          });
        if (last) {
          assert.equal(newId, id);
          done();
        }
      });

  });

});
