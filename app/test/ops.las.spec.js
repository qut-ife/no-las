'use strict';

const assert = require('assert');
const lasOps = require('../src/operations/las');

describe('LAS', function () {



  it('should split version into an object', function (done) {
    const line = `WRAP  .    NO                                      :One Line per Depth step`;

    let ver = lasOps.getMNEM(line);

    assert.deepEqual(ver, {
      mnem: 'WRAP',
      units: '',
      data: 'NO',
      desc: 'One Line per Depth step'
    });
    done();
  });

  it('should split version into an object, with no description', function (done) {
    const line = `WRAP  .                                          :`;

    let ver = lasOps.getMNEM(line);

    assert.deepEqual(ver, {
      mnem: 'WRAP',
      units: '',
      data: '',
      desc: ''
    });
    done();
  });

  it('should split version into an object, version', function (done) {
    const line = `VERS  .    2.0                                     :CWLS Log ASCII Standard - VERSION 2.0`;

    let ver = lasOps.getMNEM(line);

    assert.deepEqual(ver, {
      mnem: 'VERS',
      units: '',
      data: '2.0',
      desc: 'CWLS Log ASCII Standard - VERSION 2.0'
    });
    done();
  });


  it('should split well into an object', function (done) {

    const line = `STOP            .m                    715.16240            :STOP DEPTH`;

    let wel = lasOps.getMNEM(line);

    assert.deepEqual(wel, {
      mnem: 'STOP',
      units: 'm',
      data: '715.16240',
      desc: 'STOP DEPTH'
    });
    done();

  });

  it('should split well into an object', function (done) {
    const line = 'LOC  .        LAT:25°55"28.2\' S, LONG:150°05"25.6\' E : Location';
    let wel = lasOps.getMNEM(line);

    assert.deepEqual(wel, {
      mnem: 'LOC',
      units: '',
      data: 'LAT:25°55"28.2\' S, LONG:150°05"25.6\' E',
      desc: 'Location'
    });
    done();

  });

  it('should split curve into an object', function (done) {

    const line = `TENS            .lbf                                       :        (WLWorkflow)            (6in)                   Cable Tension`;

    let wel = lasOps.getMNEM(line);

    assert.deepEqual(wel, {
      mnem: 'TENS',
      units: 'lbf',
      data: '',
      desc: '(WLWorkflow)            (6in)                   Cable Tension'
    });
    done();

  });


  it('should split curve into an object (2)', function (done) {

    const line = `RXO8            .ohm.m                                     :        (HDRS-B)                (2in)                   Invaded Formation Resistivity filtered at 8 inches`;

    let wel = lasOps.getMNEM(line);

    assert.deepEqual(wel, {
      mnem: 'RXO8',
      units: 'ohm.m',
      data: '',
      desc: '(HDRS-B)                (2in)                   Invaded Formation Resistivity filtered at 8 inches'
    });
    done();

  });



});
