'use strict';

const log = require('../src/logger')();
const settings = require('../settings');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const assert = require('assert');
const parse = require('../src/controllers/parse');
const las = require('../src/controllers/las');
const lasOps = require('../src/operations/las');
const ascii = require('../src/controllers/ascii');

const fileDir = process.cwd() + '/test/test-data/';
const fileLocation = fileDir + '0_Origin_Condabri_23_AIT-PEX-GPIT_Main_Pass_Final.las';

describe('LAS', function () {

  it('should load each line to process', function (done) {

    let id = null;
    let newId = undefined;
    let amend = {
      file: fileLocation,
      version: [],
      well: [],
      curve: [],
      parameter: [],
      other: [],
      ascii: []
    };

    let curve = [];

    const start = new Date().getTime();

    log.debug('file: ' + fileLocation);
    las
      .create()
      .then((res) => {
        id = res.id;
        let previousSection = null;
        log.debug('parse Line By Line');
        parse.lineByLine(fileLocation, function (line, last) { // eslint-disable-line no-unused-vars
          let section = lasOps.getSection(line);
          if (section) {
            if (previousSection !== section) {
              previousSection = section;
            }
          }
          if (previousSection !== section && !line.startsWith('#')) {
            amend[previousSection].push(line);
          }
        })
          .then(() => {
            log.debug('parse las');
            return parse.las({
              status: 'new',
              file: amend.file,
              version: amend.version,
              curve: amend.curve,
              other: amend.other,
              parameter: amend.parameter,
              well: amend.well
            });
          })
          .then((res) => {
            log.debug('insert las');
            curve = res.curve;
            return las.insert(id, res);
          })
          .then((res) => {
            log.debug('partial ascii parse');
            //do partial inserts into ascii table
            return parse.ascii(res.id, curve, amend.ascii);
          })
          .then((res) => {
            return res.map((asciiObject) => {
              log.debug('partial ascii insert into db');
              return ascii.insert(mongoose.Types.ObjectId(), asciiObject, true);
            });
          })
          .then((res) => {
            log.debug('insert promise all');
            return res.reduce((p, f) => {
              return p
                .then(f)
                .catch((err) => {
                  console.log(err); // eslint-disable-line no-console
                  reject(err);
                });
            }, Promise.resolve());
            //return Promise.all(res);
          })
          .then((res) => {
            const end = new Date().getTime();
            const t = end - start;
            log.debug('import time: ' + t + 'ms');
            assert.equal(res[0], null);
            done();
          })
          .catch((error) => {
            const end = new Date().getTime();
            const t = end - start;
            log.error('import error time: ' + t + 'ms');
            log.error(error);
            console.log(error); // eslint-disable-line no-console
            done();
          });
        if (last) {
          assert.equal(newId, id);
          done();
        }
      });

  });

});
