'use strict';

const settings = require('../settings');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect(`mongodb://${settings.db_url}/${settings.db_name}`);

const assert = require('assert');
const parse = require('../src/controllers/parse');
const prelas = require('../src/controllers/prelas');
const las = require('../src/controllers/las');

describe('parse LAS', function () {

  it('should parse prelas object and insert into db', function (done) {

    const id = '5848d2fe95de7c090827afe7';
    prelas
      .getById(id)
      .then((res) => {
        //insert las withouth asciis
        let newLas = parse.processPrelas(res);
        return las.insert(id, newLas, true);
      })
      .then((res) => {
        //insert asciis with res.id into las_id and order number
        let newProlasASCII = parse.processPrelasASCII(res);
        return las.insert(id, newProlasASCII, true);
      })
      .then((res) => {
        assert.equals(res.ascii[0].data[0], '9.98220');
        done();
      })
      .catch((error) => {
        console.log('prelas object and insert into db: ' + error); // eslint-disable-line no-console
        done();
      });

  });


});

describe.only('Partial insert and parse of asciis', function () {

  const id = '5848e5653e7b8323dda4a1a0';
  it('should partialy insert an ascii', function (done) {

    //insert LAS

    //get LAS ID
    prelas
      .getById(id)
      .then((res) => {
        const ps = parse.ascii(id, res.asciis);

        Promise.all(ps).then(() => {
          assert.equal(null, null);

        }).catch((error) => {
          console.log(error); // eslint-disable-line no-console
        });
        done();
      });
  })
})

