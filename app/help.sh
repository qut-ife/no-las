#!/bin/bash
echo npm run files:clear
echo will delete files table in the database
echo ----
echo npm run lasASCII:clear
echo Will delete LAS and ASCII tables
echo ----
echo Load files to process into database
echo Modify arguments on command to process that directory
echo npm run files:prepare \'/las/data/Bowen-Surat\'
echo ----
echo npm run process:las yes yes
echo Will process all files that load LASs and ASCIIs
echo 2 arguments to include other [1] and parameters [2] of the LASs files
