//TODO: add env vars here
require('dotenv').config({path: __dirname + '/.env'});

module.exports = {
  db_url: process.env.db_url || 'localhost:27017',
  db_name: process.env.db_name || 'no-las',
  log_file: process.env.log_file || '/Users/moy/Node/Source/no-las/app/logs/log.out',
  log_level:process.env.log_level || 'DEBUG',
  las_dir:process.env.las_dir || '/Users/moy/Source/Node/no-las/app/test/test-data'
};

