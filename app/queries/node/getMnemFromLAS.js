//Gets depth of a LAS from a determined range

'use strict';

var MongoClient = require('mongodb').MongoClient;
var assert = require('assert');
var ObjectID = require('mongodb').ObjectID;

var fileLocation = '/Users/moy/Source/Node/no-las/app/test/test-data/BLD HALIFAX 1_mem 2608-4267m.las';
var mnem = 'DEPTH';

const settings = require('../../settings');

var url = `mongodb://${settings.db_url}/${settings.db_name}`;
var asciiss = [];

MongoClient.connect(url, function (err, db) {
  assert.equal(null, err);

  getLAS(db, fileLocation, function (las) {

    getASCII(db, las, function (as) {

      as.map(function (ascii) {

        ascii.ascii.map(function (cur) {
          if (cur.mnem === mnem) {
            asciiss = asciiss.concat(cur.data);
          }
        });

        asciiss;

      });

    });

    db.close();
  });

});


var getLAS = function (db, fileLocation, cb) {

  var lass = db.collection('las');

  lass
    .find({'file': fileLocation})
    .toArray(function (err, docs) {
      assert.equal(err, null);
      cb(docs[0]);
    });

};

var getASCII = function (db, las, cb) {

  var asciis = db.collection('asciis');

  var id = ObjectID(las._id).toString();

  asciis
    .find({'las_id': id.toString()})
    .toArray(function (err, docs) {
      assert.equal(err, null);
      cb(docs);
    });

};
