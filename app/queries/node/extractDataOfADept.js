/*
 Moises Sacal

 Standalone script for getting all values from a DEPTH range
 requirements;
 - npm modules: assert, underscore, mongodb, csv-write-stream, dotenv
 - settings environment vars in ../../settings.js
 - db_url, db_name
 Usage:
 node /path/extractDataOfADept.js [1] [2] [3] [4] [5] [6] [7] [8]
 1: File in LAS database
 2: Min (Inclusive)
 3: Max (Exclusive)
 4: Dept string
 5: Dept direction; optional; default UP)
 6: Output file location directory; optional: default is current working directory)
 7: Connection Timeout MS; optional; default: 100000
 8: Socket Timeout MS; optional; default: 100000

 */
/* eslint no-console:0 */

'use strict';

const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
const _ = require('underscore');
const ObjectID = require('mongodb').ObjectID;
const fs = require('fs');
const csv = require('ya-csv');
const settings = require('../../settings');

const getFile = (url, fileName, cb) => {

  MongoClient.connect(url, function (err, db) {
    assert.equal(null, err);
    getLAS(db, fileName, function (las) {
      if (las) {
        getASCII(db, las, function (as) {
          db.close();
          cb(joinAsciis(as));
        });
      } else {
        db.close();
        console.log('File: ' + fileName + ' Not Found');
      }
    });
  });
};

const joinAsciis = function (aAsciis) {

  if (aAsciis.length <= 1) {
    return _.first(aAsciis).ascii;
  } else {
    const mnemA = [];

    _.first(aAsciis).ascii.map((ascii) => {
      mnemA.push({mnem: ascii.mnem, data: []});
    });

    aAsciis.map((a) => {
      mnemA.forEach((m, index) => {
        mnemA[index].data = mnemA[index].data.concat(getMNEMDATA(a.ascii, m.mnem));
      });
    });

    return mnemA;
  }

};

const getLAS = function (db, fileName, cb) {

  const lass = db.collection('las');

  lass
    .find({'file': {$regex: fileName}})
    .toArray(function (err, docs) {
      assert.equal(err, null);
      cb(_.first(docs));
    });

};

const getASCII = function (db, las, cb) {

  const asciis = db.collection('asciis');
  const id = ObjectID(las._id).toString();

  asciis
    .find({'las_id': id}, {})
    .toArray(function (err, docs) {
      assert.equal(err, null);
      docs.sort((a, b) => {
        return a.order - b.order
      });

      cb(docs);
    });

};

const getMNEMDATA = (ar, mnem) => {
  let val = ar.find(function (obj) {
    return obj.mnem.toUpperCase() === mnem.toUpperCase();
  });
  assert.notEqual(val, undefined, 'Cannot find "' + mnem + '"');
  return val.data;
};

const getIndexOf = (array, value) => {
  return array.findIndex((el) => {
    return el >= value;
  })
};

const getIndex = (array, min, max) => {
  array = array.map((a) => parseFloat(a));
  const minI = array.reduce((a, cV, cI) => cV <= min ? cI : a, 0);
  const maxI = array.reduce((a, cV, cI) => max >= cV ? cI : a, 0);

  return {
    min: minI,
    max: maxI
  }

};

const reverse = (asciis) => {

  return asciis.map((ascii) => {
    ascii.data = ascii.data.reverse();
    return ascii;
  });

};

const extractDataOfADept = (url, args, cb) => {

  getFile(url, args.fileLocation, (asciis) => {
    if (asciis.length === 0) {
      cb('No ASCII Data Found', [], 0);
    } else {
      if (args.deptDir === 'DOWN') {
        asciis = reverse(asciis);
      }
      let index = getIndex(getMNEMDATA(asciis, args.dept), args.min, args.max);

      if (index.min === -1) {
        console.log('Min value: ' + args.min + ' not found');
        process.exit(1);
      }
      if (index.max === -1) {
        console.log('Max value: ' + args.max + ' not found');
        process.exit(1);
      }

      let asciiRange = asciis.map((obj) => {
        return {
          mnem: obj.mnem,
          data: obj.data.slice(index.min, index.max)
        }
      });

      const dataLength = _.first(asciiRange).data.length;

      cb('Find File Complete', asciiRange, dataLength);
    }
  });

};

const showUsage = () => {
  console.log('Usage:');
  console.log('node extractDataOfADept [1] [2] [3] [4] [5] [6] [7] [8]');
  console.log('1: File');
  console.log('2: Min (Inclusive)');
  console.log('3: Max (Exclusive)');
  console.log('4: Dept name (dept or depth)');
  console.log('5: Dept Direction (default up; i.e. 250, 251, 252...) (optional)');
  console.log('6: Output file location (optional)');
  console.log('7: Connect Timeout MS (optional)');
  console.log('8: Socket Timeout MS (optional)');
};


const validateArguments = (args) => {
  if (args.fileLocation === null || typeof args.fileLocation !== 'string') {
    console.log('File needed; got ' + args.fileLocation + ' instead');
    process.exit(1);
  }
  if (args.min === null || typeof args.min !== 'number') {
    console.log('Min needed, must be a number got ' + process.argv[3] + ' instead');
    process.exit(1);
  }
  if (args.max === null || typeof args.max !== 'number') {
    console.log('Max needed, must be a number' + process.argv[4] + ' instead');
    process.exit(1);
  }
  if (args.max <= args.min) {
    console.log('Max: ' + args.max + ' is lower or equal to Min: ' + args.min);
    process.exit(1);
  }
  if (args.dept === null || typeof args.dept !== 'string') {
    console.log('DEPT or DEPT needed, must be string; got ' + process.argv[5] + ' instead');
    process.exit(1);
  }
  if (args.deptDir !== 'DOWN' && args.deptDir !== 'UP') {
    console.log('Direction of dept should be up or down');
    process.exit(1);
  }
  if (process.argv[5] === null) {
    console.log('File will be stored ' + args.loc);
  } else {
    console.log('File to be stored: ' + args.loc);
  }
  if (typeof args.loc !== 'string') {
    console.log('Location in incorrect format');
    process.exit(1);
  }
  if (typeof args.connectTimeoutMS !== 'number') {
    console.log('ConnectTimeoutMS in incorrect format');
    process.exit(1);
  }
  if (typeof args.socketTimeoutMS !== 'number') {
    console.log('SocketTimeoutMS in incorrect format');
    process.exit(1);
  }

};


//START OF PROGRAM

const args = {
  fileLocation: process.argv[2],
  min: parseFloat(process.argv[3]),
  max: parseFloat(process.argv[4]),
  dept: process.argv[5],
  deptDir: (process.argv[6] || '').toUpperCase() || 'UP',
  loc: process.argv[7] || process.cwd() + '/file.csv',
  connectTimeoutMS: process.argv[8] || 100000,
  socketTimeoutMS: process.argv[9] || 100000
};

const url = `mongodb://${settings.db_url}/${settings.db_name}?connectTimeoutMS=${args.connectTimeoutMS}&socketTimeoutMS=${args.socketTimeoutMS}`;
console.log('Connection url: ' + url);

showUsage();

validateArguments(args);

console.log(`
Extract Data Of A Dept
From: ${args.fileLocation}
Min: ${args.min}, Max: ${args.max}
Dept: ${args.dept}
Dept Direction: ${args.deptDir}
Location: ${args.loc}
Connection Timeout MS: ${args.connectTimeoutMS}
Socket Timeout MS: ${args.socketTimeoutMS}
`);

extractDataOfADept(url, args, (msg, asciiRange, dataLength) => {
  console.log(msg);
  if (dataLength <= 0) {
    console.log('No data found');
    process.exit(1);
  } else {
    const headers = [];
    asciiRange.map((obj) => {
      headers.push(obj.mnem);
    });

    const writer = csv.createCsvFileWriter(args.loc, {'flags': 'w'});
    writer.writeRecord(headers);

    for (let i = 0; i < dataLength; i++) {
      const row = [];
      asciiRange.map((obj) => {
        row.push(obj.data[i]);
      });
      writer.writeRecord(row);
    }
    writer.writeStream.end(() => {
      console.log('Extract Data Complete');
      process.exit(1);
    });

  }

});

