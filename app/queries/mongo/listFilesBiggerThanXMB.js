//Query that shows files bigger than sizeInBytes and status is not equal to success.

db = db.getSiblingDB('no-las');

//1 Byte = 9.5367431640625e-07 Mb

var byteToMb = 9.5367431640625e-07;

var sizeInMb = 0.01;
var collection = db.getCollection('files');

var col = collection.find({
  "status": {$ne: "success"},
  "info.size": {$gte: sizeInMb/byteToMb}
}).map(function(c){
  var s = c.info.size || 0;
  return 'id: ' + c._id + ' size:' + (s * byteToMb).toFixed(2) + ' Mb, ' + c.fileLocation;
});

col;

print('Files greater than or equal than: ' + sizeInMb + ' Mb = ' + col.length + ' files');

// And sum the size of all uncompressed LAS
var s = 0;

collection.find({}).forEach(function(f){
  s += parseFloat(f.info.size);
});

print('All Files: ' + (s * byteToMb).toFixed(2)  + ' Mb');
