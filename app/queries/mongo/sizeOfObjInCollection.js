//Get size in MB of objects in selected collection

db = db.getSiblingDB('no-las');

var col = db.getCollection('asciis');
var total = 0;

col.find().forEach(function (obj) {
  var size = Object.bsonsize(obj);
  print('_id: ' + obj._id + ' || Size: ' + size + 'b -> ' + Math.round(size / (1024)) + 'Kb -> ' + Math.round(size / (1024 * 1024)) + 'Mb (max 16Mb)');
  total += Math.round(size / (1024 * 1024));
});

print('total: ' + total);
