//Example of List of all LASs with each belonging ASCII

db = db.getSiblingDB('no-las');

var las = db.getCollection('las').find({}, {_id: 1}).map(function (l) {
  return l._id.valueOf();
});

las.map(function (l) {
  return {
    las_id: l,
    asciis_id: db.getCollection('asciis').find({las_id: l}, {
      _id: 1,
      order: 1,
      total: 1
    }).map(function (as) {
      return {
        id: as._id,
        order: as.order
      }
    })
  }
});

