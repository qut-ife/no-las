db = db.getSiblingDB('no-las'); //use no-las

db.loadServerScripts();

var lass = db.getCollection('las');

var las = lass.find({}, {well: 1, parameter: 1});

//var latis = las.map(function(l){ return l.well.find(function(o){return o.mnem === 'LATI'}) } );
//latis.map(function(l){return l;})

var res = las.map(function (l) {
  var lat = getMNEM(l.well, ['LATI', 'LAT', 'LL1']) || null;

  if (!lat) {
    lat = getMNEM(l.parameter, ['LATITUDE', 'LATD', 'LAT', 'LATI']) || null;
  }

  var lng = getMNEM(l.well, ['LONG', 'LNG', 'LL2', 'WEST']) || null;
  if (!lng) {
    lng = getMNEM(l.parameter, ['LONGITUDE', 'LONGD', 'LONG', 'LON', 'LNG']) || null;
  }

  return {
    las_id: l._id,
    lat: lat,
    lng: lng,
    loc: getMNEM(l.well, ['FL1', 'LOC']) || getMNEM(l.parameter, ['FL1', 'LOC']),
    loc2: getMNEM(l.well, ['FL2', 'LOC2']) || getMNEM(l.parameter, ['FL2', 'LOC2']),
    // n: getMNEM(l.well, ['NORTH', 'NRTH']) || null,
    // s: getMNEM(l.well, ['SOUTH', 'STH']) || null,
    // e: getMNEM(l.well, ['EAST', 'EST']) || null,
    // w: getMNEM(l.well, ['WEST', 'WST']) || null
  }
});

var arr = [];

res.forEach(function (r) {
  if (!r.lat || !r.lng) {
    arr.push(r.las_id);
  } else if (!r.lat && !r.lng && !r.loc) {
    arr.push(r.las_id);
  }
});

print(arr);

print('Nulls:' + arr.length);

var coords = db.getCollection('coords');

coords.drop();

db.coords.insert(res);

