//GETS a MNEM object from a fileLocation
//Same as getASCIIMNEMfromFile.js but shorter version for faster reading

db = db.getSiblingDB('no-las');

var fileLocation = 'AAO PLEASANT HILLS 4_LAS/AAO PLEASANT HILLS 4_USIT-CBL-GR-CCL-HighResPass.las';
var MNEM = 'DEPT';

db.getCollection('las').find(
  {file: {$regex: fileLocation}},
  {_id: 1})
  .map(function (m) {
    return db.getCollection('asciis').findOne({las_id: m._id.valueOf()});
  }).map(function (a) {
  return a.ascii;
}).map(function (ascii) {
  return ascii.find(function (a) {
    return a.mnem == MNEM;
  });
});
