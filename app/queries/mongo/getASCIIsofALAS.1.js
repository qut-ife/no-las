//Get ASCIIs of a LAS fileLocation

db = db.getSiblingDB('no-las');

var fileName = 'SSL SPRINGWATER 13_CLIENT/SSL SPRINGWATER 13_DATA/SSL SPRINGWATER 13_RUN2_PEX-HRLA-BHC_TD TO CS.las';

db.getCollection('las').find({file: {$regex: fileName}}).map(function (m) {
  return db.getCollection('asciis').find({las_id: m._id.valueOf()}, {
    _id: 1,
    order: 1,
    total: 1
  }).sort({order: 1}).map(function (as) {
    return as;
  });
});
