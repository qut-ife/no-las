//Create Index LAS file

db = db.getSiblingDB('no-las');

db.getCollection('las').createIndex(
  {file: 1},
  {name: "file_index", collation: {locale: "en", strength: 2}}
);
