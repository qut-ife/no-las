//Save function into system

db = db.getSiblingDB('no-las');

//getMNEM: gets data from mnem [String] from ar [array]
db.system.js.save({
  _id: "getMNEM",
  value: function (ar, mnem) {
    var ret = mnem.map(function (mn) {
      var param = ar.find(function (obj) {
        return obj.mnem.toUpperCase() === mn.toUpperCase();
      });
      if (param != undefined && param.data != undefined) {
        return param.data;
      }
    });
    return ret.find(function (r) {
      return r != undefined;
    });
  }
});
