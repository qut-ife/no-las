//Create Index LAS file

db = db.getSiblingDB('no-las');

db.getCollection('asciis').createIndex(
  {order: 1},
  {name: "order_index", collation: {locale: "en", strength: 2}}
);
