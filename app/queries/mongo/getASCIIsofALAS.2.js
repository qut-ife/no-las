//Get ASCIIs of a LAS fileLocation with IN
//This could be slower because it will have to store all las in a las object

db = db.getSiblingDB('no-las');

var fileName = 'AAO PLEASANT HILLS 4_LAS/AAO PLEASANT HILLS 4_USIT-CBL-GR-CCL-HighResPass.las';

var las = db.getCollection('las').find({file: {$regex: fileName}}, {_id: 1}).map(function (l) {
  return l._id.valueOf();
});

db.getCollection('asciis').find({las_id: {$in: las}}, {las_id: 1, _id: 1}).map(function (as) {
  return as;
});
