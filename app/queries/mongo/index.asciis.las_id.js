//Create Index ASCIIS las_id

db = db.getSiblingDB('no-las');

db.getCollection('asciis').createIndex(
  {las_id: 1},
  {name: "las_id_index", collation: {locale: "en", strength: 2}}
);
