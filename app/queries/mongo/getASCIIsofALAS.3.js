//Get ASCIIs of a LAS fileLocation

db = db.getSiblingDB('no-las');

db.getCollection('las').find({file: {$regex: 'ZEROGEN 10 LAS/ZEROGEN 10 WAVEFORM_MAINLOG.las'}}, {_id: 1}).map(function (l) {
  return l._id.valueOf()
}).map(function (l) {
  return db.getCollection('asciis').find({las_id: l}, {_id: 1}).map(function (r) {
    return r;
  })
});

//Since this result is larger than 32 mb we are only showing _id in the projection
