##Commands

1. `npm run files:clear`
 
    Will delete files table in the database

2. `npm run lasASCII:clear` 

    Will delete LAS and ASCII tables

3. `npm run files:prepare '/las/data/Bowen-Surat'`  
    
    Will
    - Load files to process into database
    - Modify arguments on command to process that directory

4. `npm run process:las yes yes`
 
    Will
    - process all files that load LASs and ASCIIs
    - 2 arguments to include other [1] and parameters [2] of the LASs files

5. `npm run query.extractDataOfADept "AGA CUBAN 1 LAS_1.las" 70 99 "Bit DEPTH"`
    
     Standalone script for getting all values from a DEPTH range
     requirements;
     
     - npm modules: assert, underscore, mongodb, csv-write-stream, dotenv
     - settings environment vars in ../../settings.js
     - db_url, db_name
    
    Will extract data of a depth with 4 arguments 
    
     - File in LAS database
     - Min (Inclusive)
     - Max (Exclusive)
     - Dept string (dept or depth in most cases)
     - Output file location directory (optional)
     
6.  `npm run mongo:createCoords`

    Will create a coords collection with all posible coords with las_id
    
7.  `npm run process:cleanCoords`    

    Will parse coords into a location collection with the las_id
