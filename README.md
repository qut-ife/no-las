## NO-LAS

LAS to Mongo database conversion using Node.JS

These set of scripts are designed to build up a mongo database from a directory containing LAS files version 2.0.
It leverages the collection to parse in memory ascii files and store them in a semi-relationship manner.

FILE =- LAS =- ASCII

- LAS Collection:
    - Metadata of LAS file.
    - VERSION, CURVE, PARAMETERS, OTHER arrays.
    - Version, Curve and Parameters are arrays of mnemonics.
    - file_id one to one relationship with FILE collection.
- ASCII Collection:
    - Data abstraction of LAS collection with `las_id` relationship. 
    - A LAS file has a zero to many relationship with ASCII collection.
    - ASCII compiled of an array of CURVE information. 
- FILE Collection:
    - Representation of files in the system with parse status i.e. `{success: true}` as in parsing has been successful.
    - Other file info like size, location, etc.

For information on LAS Spec see [LAS_20_Update_Jan2014.pdf](docs/LAS_20_Update_Jan2014.pdf)

Folder tree:

- [app](app/): Application 
    - logs execution logs with the following format
        - DATE - LOG LEVEL - INFO
        - To setup the log level modify a `.env` file with log_level= TRACE | DEBUG | ERROR | INFO
    - [src](app/src/): All execution source
        - For available scripts to end user see the script section in [package.json](../app/src/package.json) file
    - [queries](app/queries/): Queries in Mongo Shell and Node.JS 
    - [test](app/test/): Execute unit test with mocha
    - [help.sh](app/help.sh): to run `source app/help.sh` to show basic help
- [db](db/): Database folders and running helpers
    - data (create folder)
        - db folder for mongo
        - log mongo log
    - scripts for running mongo
- [docs](docs/): Documentation
    - [queries](docs/Queries.md) Some example queries
    
#### Installation

```
npm install
```

