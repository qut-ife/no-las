### Queries

*All queries and/or scripts are run at `app` directory level*

#### Save functions 
- `npm run mongo:saveFunctions`
- Functions can be stored in the database for future use 
- See [saveFunction.js](../app/queries/mongo/saveFunction.js)
  
#### Extract Data of a Dept 
- `node queries/node/extractDataOfADept "Cased Hole Logs/Pleasant Hills 5 CBL3a 17-AUG-11.las" 1000 1013 "DEPT" "down" "/home/debian/file.csv"`
- `node queries/node/extractDataOfADept "ZEROGEN 10 LAS/ZEROGEN 10 WAVEFORM_MAINLOG.las" 1000 1013 "DEPT" "UP" "/home/debian/file.csv"`    
- `node queries/node/extractDataOfADept.js "/las/data/Bowen-Surat/1990-1999/ANU SOUTHEAST TEATREE 1/BGTM.LAS" 0 100 "DEPT" "DOWN" "/home/debian/file.csv"`
- Standalone script for getting all values from a DEPTH range 
    - Requirements;
        - npm modules: assert, underscore, mongodb, csv-write-stream, dotenv
        - settings environment vars in ../../settings.js
        - db_url, db_name
  
    - Will extract data of a depth with 8 arguments 
        - File in LAS database 
            - Script will try to find the best file. Keep in mind that file names could be repeated. Try full path name.
        - Min (Inclusive)
        - Max (Exclusive)
        - Dept string
            - Some LAS files have different name for DEPT
        - Dept direction; optional; default UP) 
            - Set direction of DEPT on file (Options UP | DOWN)
        - Output file location directory; optional: default is current working directory)
        - Connection Timeout MS; optional; default: 100000
        - Socket Timeout MS; optional; default: 100000
- Script in [extractDataOfADept.js](../app/queries/node/extractDataOfADept.js)

#### Process coords 
- `npm run mongo:createCoords`
    - This mongo script will extract the coordinates available in all las files
    - For more see [Location.md](Location.md)
    
#### Clean `coords` collection into `location` collection 
- `npm run process:cleanCoords`
- Will parse coordinates into a location collection with the las_id

#### Other queries
- `npm run mongo:sizeOfObjInCollection`: Get sizes of stored collection
- `npm run mongo:getASCIIsofLAS`: Get an ASCII of a LAS object
- `npm run mongo:listFilesBiggerThanXMB`: List all files larger than a defined size
- `npm run mongo:updateExample`: Update a LAS file with a query
- `npm run mongo:createIndex`: Create an index of an object
- `npm run files:clear`: Will delete files table in the database
- `npm run lasASCII:clear`: Will delete LAS and ASCII tables
  
  
