### Requirements

* This app was tested with Debian and MacOS
* Mongo 3 and above
* Node.JS 6
* NPM 3

### Run

* Install [Mongo Comunity Server 64bits](https://www.mongodb.com/download-center#community), 
* [Node.JS & NPM](https://nodejs.org/en/download/) 
* Some DB helpful start scripts are stored in [db](../db/)
* All node.js and mongo scripts can be run with `npm run [script]` See: [package.json](../app/package.json)

### Execution Order

* Start MONGO (you dont need to create a no-las collection)
* Create .env (environment variables) file in the app folder with:

```
db_url=localhost:27017
db_name=no-las
log_file=/Users/user/logs/log.out
log_level=DEBUG
las_dir=/Users/user/las-files
```

#### Prepare Files 
* `npm run files:prepare '/las/data/Bowen-Surat'`  
* This will start a recursive search of all files in the directory that have .las extension
* It will create a database collection with all .LAS as `files`
  
#### Process LAS collection 
* `npm run process:las yes yes`
* Two arguments are needed to include other [1] and parameters [2] of the LASs files
* Select which folder you wish to select all .LAS files
* Will read all `files` collection that do not have a `status:'success'` and start parsing
* Errors can happen, for example:  
 - A LAS file
   - A LAS should contain all mandatory sections:
     - ~V, ~A, ~W, ~C
   - These next sections are optional:
     - ~P, ~O
    
* All required sections must be in the form of a `MNEM`
 + A `MNEM` object is defined by:
    
```
mnem = {
  "desc" : "description",
  "data" : "data",
  "units" : "unit",
  "mnem" : "name of mnemonic"
}
```
    
* If the file is valid, see next BSON LAS Example:
  
```
{
  "_id" : ObjectId("588ac05699fbe93170caf3e3"),
  "status" : "success",
  "updatedOn" : ISODate("2017-01-27T03:36:54.965Z"),
  "createdOn" : ISODate("2017-01-27T03:36:54.965Z"),
  "other" : [],
  "parameter" : [],
  "curve" : [
     {
        "desc" : "Depth Index",
        "data" : "",
        "units" : "m",
        "mnem" : "DEPT",
        "_id" : ObjectId("588ac05799fbe93170caf41b")
      },
      ...
  ],
  "well" : [
    {
      "desc" : "START DEPTH",
      "data" : "214.88400",
      "units" : "m",
      "mnem" : "STRT",
      "_id" : ObjectId("588ac05799fbe93170caf411")
    },
     ...
  ],
  "version" : [
    {
        "desc" : "One Line per Depth step",
        "data" : "NO",
        "units" : "",
        "mnem" : "WRAP",
        "_id" : ObjectId("588ac05799fbe93170caf3e9")
    }, 
    ...
  ],
  "__v" : 0,
  "file_id" : "588abcbe56f01c3028745aee",
  "file" : "/las/data/Bowen-Surat/1960-1969/AAO PLEASANT HILLS 4/AAO PLEASANT HILLS 4_Client/AAO PLEASANT HILLS 4_LAS/AAO PLEASANT HILLS 4_USIT-CBL-GR-CCL-HighResPass.las"
}
```
  
* The program will check if in version the WRAP element is NO or YES
 + If in section version the WRAP option is WRAP:YES then the ASCII data is split differently to accomodate old machines
    
 + Example:
    
```
~CURVE
DEPT.M:depth
curve1
curve2
curven

~A
DEPT1
curve1 curve2 curve3
curve4 curve...n
DEPT2
curve1 curve2 curve3
curve4 curve...n
```

```
~ASCII Log Data
   760.000
       1064.453         40.496         50.523         70.078          8.181
          6.369         -0.376          2.560          0.002          0.000
          5.442          6.125         33.352         35.525          2.564

   760.100
       1030.273         40.635         50.527         75.398          8.251
          6.368         -0.372          2.562          0.002          0.000
          5.309          6.125         33.501         35.677          2.565

  ...
```
    
* The program will store all ascii in the `asciis` collection with a las_id identifying it. Due to memory restrictions
in MONGO an ASCII can be split into 1 or more `ascii` collection object
    
```
{
    "_id" : ObjectId("588ac05799fbe93170caf41c"),
    "__v" : 0,
    "status" : "success",
    "createdOn" : ISODate("2017-01-27T03:36:55.196Z"),
    "updatedOn" : ISODate("2017-01-27T03:36:55.196Z"),
    "ascii" : [
        {
            "mnem" : "DEPT",
            "_id" : ObjectId("588ac05799fbe93170caf426"),
            "data" : []
        },
        {
            "mnem" : "TENS",
            "_id" : ObjectId("588ac05799fbe93170caf425"),
            "data" : []
        },
        {
            "mnem" : "RSAV",
            "_id" : ObjectId("588ac05799fbe93170caf424"),
            "data" : []
        },
        {
            "mnem" : "CS",
            "_id" : ObjectId("588ac05799fbe93170caf423"),
            "data" : []
        },
        {
            "mnem" : "CCL",
            "_id" : ObjectId("588ac05799fbe93170caf422"),
            "data" : []
        },
        {
            "mnem" : "CBL",
            "_id" : ObjectId("588ac05799fbe93170caf421"),
            "data" : []
        },
        {
            "mnem" : "CBSL",
            "_id" : ObjectId("588ac05799fbe93170caf420"),
            "data" : []
        },
        {
            "mnem" : "TT",
            "_id" : ObjectId("588ac05799fbe93170caf41f"),
            "data" : []
        },
        {
            "mnem" : "TTSL",
            "_id" : ObjectId("588ac05799fbe93170caf41e"),
            "data" : []
        },
        {
            "mnem" : "GR",
            "_id" : ObjectId("588ac05799fbe93170caf41d"),
            "data" : []
        }
    ],
    "total" : 1,
    "order" : 1,
    "las_id" : "588ac05699fbe93170caf3e3"
}
```
* All `data` on each `MNEM` element is an array of strings
        

#### Errors
* Errors in LAS files and in programming happen and are and will be added in: [Errors.md](Errors.md)


#### Mongo
* Some mongo help see: [Mongo.md](Mongo.md)


#### Queries
* For some example queries see: [Queries.md](Queries.md)

