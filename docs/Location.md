## Cleaning and Parsing Location

Some locations come straight up as LATI, LONG in the format, some do not
Get LAS by Coordinates

With a query you should get an array of coords with the id of the LAS file

- Find all las well that have LAT, LONG, LATI, LON (see: [wellsInArea.3.js](../app/queries/mongo/wellsInArea.3.js)) 
 where first we get all las that have *** well $in: ['LAT', 'LONG', 'LATI', 'LON']  and well.loc *** 
 then only get well array and _id this returns an array of all wells then use a function that 
 returns only lat, lng, etc.

**These rules need to be upgraded as the LAS DB is discovered because some of them have
different formats.**
 
 Example of some options of arrays obtained with this query:
 
```javascript
 var coords: [
   {
       "las_id" : ObjectId("586bab5419c5c1160b728e0c"),
       "lat" : "25 38' 48.5'' S",
       "lng" : "148 57' 57.1'' E",
       "loc" : "PL 91"
   },
   {
       "las_id" : ObjectId("586bab7819c5c1160b728f26"),
       "lat" : null,
       "lng" : null,
       "loc" : "25* 38' 40.82\" S, 148* 59' 49.46\" E"
   },
   {
       "las_id" : ObjectId("586c45df8082c720bf0cba3b"),
       "lat" : "25* 43' 27.1\" S",
       "lng" : "148* 51' 22.3\" E",
       "loc" : "PL-92"
   },
   {
       "las_id" : ObjectId("586c829352c02b246061ddf2"),
       "lat" : "025 03' 53.900\" S    DMS",
       "lng" : "149 00' 25.470\" E    DMS",
       "loc" : "PL 236"
   },
   {
       "las_id" : ObjectId("586c8ff452c02b2460629785"),
       "lat" : "25� 24' 13.81\" S",
       "lng" : "148�56' 22.23\" E",
       "loc" : "BOWEN BASIN PL 234"
   },
   {
       "las_id" : ObjectId("586ba3ec19c5c1160b7247fe"),
       "lat" : null,
       "lng" : null,
       "loc" : "26deg56'40.4\"S 149deg13'27.0\"E"
   },
   {
       "las_id" : ObjectId("586c753c0dae6c234a4e903c"),
       "lat" : "26.457437* S",
       "lng" : "150.011826* E",
       "loc" : "DSC-06"
   }
 ];
```

Create a table that has a verified lat, lng

- First create a table with lat, lng and las_id results coords
- With this table coords extract if not lat, lng then try loc and put in coords_clean table

Example query to see coords find:

```javascript
db.getCollection('coords').find({
  lat: {$in: [null]},
  lng: {$in: [null]},
  loc: {$in: [null]},
  loc2: {$in: [null]}
}).map(
  function (c) {
    var file = db.getCollection('las').findOne({_id: c.las_id}, {file: 1});
    return {
      las_id: c.las_id,
      f: file.file,
      lat: c.lat,
      lng: c.lng,
      loc: c.loc,
      loc2: c.loc2
    }
  });
```

Found around 800 objects.

Some objects have swapped description with data. Some do not have any data.

Some objects have the exact location hidden in the `~Other` section
