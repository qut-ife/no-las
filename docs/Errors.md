### Errors

1. Fix some files where not parsed properly
For example: 
/las/data/Cooper-Eromanga/2000-2009/OCA Oak Park 1/Oakpark01_logdata.las
Version.WRAP = undefinedRAP	.NO		:ONE LINE PER DEPTH STEP
The problem is:
WRAP .NO  :ONE LINE PER DEPTH STEP
It is not following the standard.
The standard is whatever is right of the dot is a unit so the fix ix to convert the line to
WRAP . NO  :ONE LINE PER DEPTH STEP
Add a space right after the dot	

How to fix it for multiple files?
Either fix the files or the database.

2. Some files seem to be repeated to process because of Lower or Upper Case

First see which ones are
Use aggregation on file and get file with count > 1:

``` javascript
db.getCollection('las').aggregate([
    {$group : { '_id': '$file', count: { $sum: 1 } } },
    {$match: {'_id' :{ $ne : null } , count : {$gt: 0} } }, 
    {$project: {'file' : '$_id', '_id' : 0} }
]).map(function(l){return l.file;});
```

3. Fix Parameter and Other to be included as optional in arguments of script

Added omit parameter and other as arguments IF needed to be excluded
So run:
```
npm run las:process yes no
```

4. Found that there is an error in the parsing of the Line

I fixed it in commit `aaef0e5` (fixed parsing line in case there is a colon : on the data part)

5. 

