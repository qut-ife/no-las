### Mongo Wiki

#### Disable Transparent Huge Pages

Transparent Huge Pages (THP) is a Linux memory management system that reduces the overhead of Translation Lookaside 
Buffer (TLB) lookups on machines with large amounts of memory by using larger memory pages.

However, database workloads often perform poorly with THP, because they tend to have sparse rather than contiguous 
memory access patterns. You should disable THP on Linux machines to ensure best performance with MongoDB.

https://docs.mongodb.com/manual/tutorial/transparent-huge-pages/

#### Backup & Restore

##### Backup
`mongodump --db no-las`

Will dump database named no-las into a folder called dump inside the executing mongodump

#### Restore
`mongorestore  ./dump`

Will restore database in dump folder into a mongod instance

#### Rsync to copy a large set of files from source to origin

`sudo rsync -Pav -e 'ssh -i ./key.pem' debian@xxx.xxx.xxx.xxx:/las/backup/dump /mnt/mongo/backup/`

#### Save a Collection to JSON and import to DataBase

Use mongoexport/mongoimport to dump/restore a collection:

Export JSON File:

`mongoexport --db <database-name> --collection <collection-name> --out output.json`

Import JSON File: 

`mongoimport --db <database-name> --collection <collection-name> --file input.json`

### Robomongo

There could be some Robomongo issues, for example, one reported in [https://github.com/paralect/robomongo/issues/1269]
where the results end in success but there is no results shown. This could mean a timeout, so, configure it in
```sh
Robomongo version 0.9.x
    /Users/<user>/.config/robomongo/0.9/robomongo.json
```
find shellTimeoutSec and increase timeout
